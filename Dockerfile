# https://www.nist.gov/image-group/nbis

FROM debian:buster as nbis

RUN apt -qq update && apt -qqy install build-essential cmake

# optional, for speed
RUN apt -qqy install libz-dev libpng-dev libtiff5 liblcms2-2	

RUN mkdir -pv /usr/local/nbis

COPY src /nbis
WORKDIR /nbis

RUN ./setup.sh /usr/local/nbis --64 --without-X11 >/dev/null

RUN make config
RUN make it
RUN make install LIBNBIS=yes
RUN find /usr/local

FROM scratch

COPY --from=nbis /usr/local/nbis /usr/local/nbis
